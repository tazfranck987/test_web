# Ressources de base pour mkdocs-material
## La doc de référence Material for Mkdocs
Cette documentation se trouve [ici](https://squidfunk.github.io/mkdocs-material/reference/){:target="_blank" }  

## Une bonne référence pour débuter
[ICI](https://tutoriels.forge.aeif.fr/mkdocs-pyodide-review/){:target="_blank" .md-button}  
Il manque juste les détails du fichier *mkdocs.yaml*.

## Une autre doc intéressante
[ICI](https://docs.framasoft.org/fr/grav/markdown.html){:target="_blank" .btn .btn-primary}  


.btn .btn-default
.btn .btn-primary
.btn .btn-info
.btn .btn-success
.btn .btn-warning
.btn .btn-danger
.btn .btn-link


## Structure minimum
Le dossier du projet mkdocs doit contenir à minima le fichier *mkdocs.yaml* (pour les métadonnées du projet) et un dossiers *docs* dans lequel on trouvera au moins le fichier *index.md*.  
Pour voir en temps réel le résultat du projet, on peut lancer un mini serveur par `mkdocs serve`. Le fichier *mkdocs.yaml* demande une attention particulière pour configurer correctement la production du site.

## Structure évoluée
On peut rajouter de nombreux outils, le plus intéressant étant *pyodide* qui permet d'insérer des IDE python dans les pages.
Une bonne documentation pour implémenter *pyodide* se trouve [ici](https://bouillotvincent.gitlab.io/pyodide-mkdocs/){:target="_blank"}.

## Emoji
[Cette page](https://squidfunk.github.io/mkdocs-material/reference/icons-emojis/){:target="_blank"} permet de trouver des emojis simples pour agrémenter le contenu.
:material-language-python:

## Petite réponse cachée
<details>
    <summary>Voir la réponse</summary>
    Voici la réponse ... ce qui revient à une admonition.
</details>

