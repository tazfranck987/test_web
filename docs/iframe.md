# Intégration d'un site externe 
(pour basthon ou autre site autorisant l'intégration.)  
Cela se réaliste avec un simple `iframe`:  
<div class="centre">
<iframe 
src=
https://compute-it.toxicode.fr/?progression=python
width="900" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>
