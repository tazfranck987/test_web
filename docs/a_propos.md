# A propos du site

Franchement !!! En 15 minutes d'explication il devient possible de faire un site statique simple et propre.


Les quatre commandes utiles à tester:


- mkdocs new [dir-name] -> Create a new project.
- mkdocs serve -> Start the live-reloading docs server.
- mkdocs build -> Build the documentation site.
- mkdocs -h -> Print help message and exit.

Par défaut la commande `mkdocs build` crée des hyperliens qui pointent vers des répertoires et non des fichiers.
Le fichier de base de chaque répertoire étant renommé `index.html` ... cela implique de disposer d'un serveur *http* type Apache ou autre dont la configuration permet de "servir" le `index.html` par défaut.
Le site statique n'est donc pas utilisable via un simple navigateur.
Heureusement, il existe une option `mkdocs build --no-directory-urls` qui permet d'imposer les liens vers des fichiers.
Cette commande permet d'utiliser le site statique sans serveur *http*.


Pour travailler avec *mkdocs*, l'IDE *pycharm* est tout à fait adapté !  

Quelques sources en vrac:  

[:arrow_forward: La chaine youtube d'un prof de NSI :arrow_backward:](https://www.youtube.com/playlist?list=PL-Q7fIakgvUAcUluPeUMIP1128wWxboJY){:target="_blank" .md-button }
<br><br>
[Le site ZoneNSI](https://www.zonensi.fr/Miscellanees/mkdocs_cmd/){:target="_blank" .md-button}

Remarque: pour ouvrir un lien dans un nouvel onglet: `[Description](url){:target="_blank" }` 

On peut aussi rajouter `.md-button` dans le même couple d'accolades pour créer un bouton.
[une autre source à explorer](https://ens-fr.gitlab.io/mkdocs/markdown-bases/){:target="_blank" }

[La démarche pour créer un site à partir de zéro, avec l'essentiel.](https://tutoriels.forge.aeif.fr/mkdocs-pyodide-review/){:target="_blank" }