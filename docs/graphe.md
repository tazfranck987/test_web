# Des arbres et graphes avec *mermaid*

```mermaid
    %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
    flowchart TB
        n0(15) --> n1(6)
        n1 --> n3(1)
        n1 --> n4(10)
        n0 --> n2(30)
        n2 --> n8[Null]
        n2 --> n5(18)
        n5 --> n6(16)
        n5 --> n7(25)
```


---
### Appels récursifs de Fibonacci(5)
```mermaid
graph TD
N1(5)
N11(4)
N12(3)
N111(3)
N112(2)
N121(2)
N122(1)
N1111(2)
N1112(1)
N1121(1)
N1122( )
N1211(1)
N1212( )
N1221( )
N1222( )


N1 --> N11
N1 --> N12
N11 --> N111
N11 --> N112
N12 --> N121
N12 --> N122
N111 --> N1111
N111 --> N1112
N112 --> N1121
N112 --> N1122
N121 --> N1211
N121 --> N1212
N122 --> N1221
N122 --> N1222

```

---
```mermaid
graph TD
A(5)
B(2)
C(8)
F( )
G( )
D(6)
E(9)
H( )
I( )
J( )
K( )
A --> B
A --> C
C --> D
C --> E
B --> F
B --> G
D --> H
D --> I
E --> J
E --> K
```

---
```mermaid
flowchart TD
    A(12) --- B(10)
    A --- C(15)
    B --- D(5)
    B --- E( )
    D --- F(4)
    D --- G(8)
    C --- H( )
    C --- I(20)
    linkStyle 3 stroke-width:0px;
    linkStyle 6 stroke-width:0px;
    style E opacity:0;
    style H opacity:0;
```

---
```mermaid
graph LR
    A( )
    B(0)
    C(1)
    D(0)
    E(1)
    F(0)
    G(1)
    A --- B
    A --- C
    B --- D
    B --- E
    C --- F
    C --- G
```

---
``` mermaid
    graph LR
    A[texte 1] ----> |50| C[texte 2];
    A -->|1| B[B];
```

---
``` mermaid
    graph LR
        A[Editeur de texte PHP] --> C[Serveur Apache]
        C --> B[Navigateur]
```


``` mermaid
    graph LR
        A ---|1| B
        B ---|5| C
        A ---|5| D
        B ---|1| D
        D ---|1| C
        A ---|50| C
```


```mermaid
    classDiagram
    class Yaourt{
        str arome
        int duree
        str genre
        __init__(arome,duree)
        modifie_duree(duree)
        modifie_arome(arome)
    }
```


[Doc mermaid](https://mermaid.live/edit#pako:eNpVkE1qw0AMha8itGohvoAXgcZus0lJodl5vBAeOTOk88NYpgTbd-84ptBqJfS-J6Q3YRc0Y4nXRNHApVYecr00lUl2EEdDC0Wxn48s4ILn-wyHp2OAwYQYrb8-b_xhhaCaTivGIMb627JJ1cN_9jxD3ZwoSojtX-XyHWZ4beyHyev_KyZxdr01PZU9FR0lqCi1uEPHyZHV-expNSgUw44VlrnV3NP4JQqVXzJKo4TPu--wlDTyDseoSbi2lB92v0PWVkJ635J4BLL8AM-3Wn4){:target="_blank" .md-button }
