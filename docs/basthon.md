# Intégration de Basthon
Les outils *Basthon*[^1] sont très pratiques ! Un interpréteur et un notebook en ligne, sans compte, sans connexion .. RGPD compatible !

[^1]: [https://basthon.fr/](https://basthon.fr/){:target="_blank" }

## Sans fichier
<div class="centre">
<iframe 
src="https://console.basthon.fr/"
width="900" height="600" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


## Avec un fichier partagé en ligne
<div class="centre" markdown="span">
<iframe 
src="https://urlz.fr/nEoR" 
width="900" height="600" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


## Un notebook Basthon
<div class="centre">
<iframe 
src="https://notebook.basthon.fr/"
width="900" height="600" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>



