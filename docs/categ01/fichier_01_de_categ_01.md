# Structure
Le fichier YAML dans la partie `nav:` permet de définir la structure du site
sans qu'elle soit pour autant liée à la structure physique des fichiers.  


par exemple:  

```
nav:
    - 'index': 'index.md'
    - 'A propos': 'a_propos.md'
    - 'Sous-catégories':
          - 'Sous-catégorie 1': 'categ01/fichier_01_de_categ_01.md'
```


![Marty Mc Fly](../images/marty.png){width=25% align=right}
Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit 
in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
deserunt mollit anim id est laborum.
<br><br><br>
!!! note "Attention à bien installer mkdocs-material avec pip !"
    ```python
    print("Tout ce qui est indenté") #
    print("sera dans la note !)
    ```
    

Et dès l'absence de l'indentation on sort de la note !


Les types de note sont: *attention, caution, danger, error, hint, important, note, tip, warning.*

??? note "Les notes peuvent être 'pliées'"
    grâce à l'extension *pymdownx.details* en remplaçant le triple point d'exclamation par un 
    triple point d'interrogation.

##Un petit test avec du python pour voir !

??? example "Exercice avec solution"
    === "Énoncé"
        Premier onglet ... on peut énoncer l'exercice avec des détails.<br>
        Et quelques indices ...
    === "Correction"
        Deuxième onglet, on peut apporter la solution à l'exrcice.  <br>
        ```python linenums='1'
        class Fraction :
            def __init__(self, num, den) :
                self.numerateur = num
                self.denominateur = den
            
            def __str__(self):
                if self.denominateur == 1:
                    return str(self.numerateur)
                return str(self.numerateur)+"/"+str(self.denominateur)
        ```
    
    === "Pour aller plus loin ..."
        Chlala etcoetera

    === "Un dernier ?"
        Voilà un dernier onglet pour voir si on est limité à 3 ou plus !


