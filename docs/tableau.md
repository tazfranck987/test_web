# Tableau de base

| Titre 1  | Titre 2 | Titre 3 |
| :---    | :----:    | ---:   |
| aligné gauche   | centré  | aligné droite |
| Pomme | Brocoli | Pois |


Pour le créer:
```commandline

| Titre 1  | Titre 2 | Titre 3 |
| :---    | :----:    | ---:   |
| aligné gauche   | centré  | aligné droite |
| Pomme | Brocoli | Pois |


```