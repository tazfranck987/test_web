# Insertion de vidéo en HTML

???+ tip "Vidéo dans une admonitions repliable"
    <div class="centre">
    <iframe 
    src="https://player.vimeo.com/video/138623558?color=b50067&title=0&byline=0&portrait=0" 
    width="640" height="360" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

<br><br><br>



<video controls src="images/BaseShadock.mp4" poster="images/shadoks.jpg">
  La base de numération shadocks
</video>



[Une video](images/BaseShadock.mp4){:target="_blank" .md-button}
