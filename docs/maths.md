---
icon: material/emoticon-happy
---
# Ecriture mathématique type LaTeX et QCM ...

soit $x\in\mathbb{R}$ tel $x<2$

Justifié à droite

$$-\sqrt{2}<x<\sqrt{2}$$

Justifié à gauche  
$a^2 + b^2 = c^2$


$$
\operatorname{ker} f=\{g\in G:f(g)=e_{H}\}{\mbox{.}}
$$