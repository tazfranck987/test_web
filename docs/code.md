On peut facilement intégrer du code python, des requêtes SQL ...  

## Du code
`du code en ligne brut` 

Avec coloration syntaxique python, et surlignage de lignes importantes.
```python hl_lines="2 5" title="tortue.py"
import turtle
turtle.fd(60)
print("C'est magnifique !!!'")
turtle.lt(90)
turtle.fd(100)
```

... ou sql
```sql
SELECT nom, prenom FROM Employes
    WHERE poste = 'chef';
```
Malheureusement je n'ai pas le petit outil pour copier le code dans le presse-papier !

<br><br>
```python 
note = float(input("Saisir votre note : "))
if note >= 16:  # (1)
    print("TB")
elif note >= 14:  # (2)
    print("B")
elif note >= 12:  # (3)
    print("AB")
elif note >= 10:
    print("reçu")
else:
    print("refusé")
```

1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

2. :warning: `elif` signifie **sinon si**.

3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

!!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"
