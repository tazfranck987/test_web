# Des QCM

## Une première méthode avec une correction proposée dans un autre onglet
Pour cet exercice sur Python, on ne regarde que **si l'identifiant est valide**, il pourrait être mal choisi.  
[La source est ici](https://ens-fr.gitlab.io/mkdocs/recettes_1/){:target="_blank" }

??? note "Exercice avec auto correction"
    === "Cocher les identifiants valides"
        - [ ] `as`
        - [x] `Roi`
        - [ ] `2ame`
        - [ ] `v413t`
        - [ ] `dix`
        - [ ] `n'œuf`
        - [ ] `huit`
        - [ ] `Sète`
        - [ ] `carte_six`
        - [ ] `_5`
        - [ ] `%4`
        - [ ] `quatre-moins-un`
        - [ ] `2!`
        - [ ] `_`
    === "Solution"
        - ❌ `as` ; c'est un mot réservé.
        - :white_check_mark: `Roi`
        - ❌ `2ame` ; interdit de commencer par un chiffre.
        - ✅ `v413t`
        - ✅ `dix`
        - ❌ `n'œuf` ; interdit d'utiliser `'`
        - ✅ `huit`
        - ✅ `Sète`
        - ✅ `carte_six`
        - ✅ `_5`
        - ✅ `_`
        - ❌ `%4` ; interdit d'utiliser `%`
        - ❌ `quatre-moins-un` ; interdit d'utiliser `-`
        - ❌ `2!` ; interdit d'utiliser `!`
        - ✅ `_`

Mais c'est peut-être mieux comme ça pour que l'élève compare confortablement ses réponses à la solution:

??? question "Exercice avec auto correction"
    === "Cocher les identifiants valides"
        - [ ] `as`
        - [x] `Roi`
        - [ ] `2ame`
        - [ ] `v413t`
        - [ ] `dix`
        - [ ] `n'œuf`
        - [ ] `huit`
        - [ ] `Sète`
        - [ ] `carte_six`
        - [ ] `_5`
        - [ ] `%4`
        - [ ] `quatre-moins-un`
        - [ ] `2!`
        - [ ] `_`
    === "Indices ..."
        Chalala


??? success "Solution de l'exercice précédent"
    === "Attention !"
        Fait bien l'effort de chercher par toi-même d'abord !
    === "La solution est là"
        - ❌ `as` ; c'est un mot réservé.
        - ✅ `Roi`
        - ❌ `2ame` ; interdit de commencer par un chiffre.
        - ✅ `v413t`
        - ✅ `dix`
        - ❌ `n'œuf` ; interdit d'utiliser `'`
        - ✅ `huit`
        - ✅ `Sète`
        - ✅ `carte_six`
        - ✅ `_5`
        - ✅ `_`
        - ❌ `%4` ; interdit d'utiliser `%`
        - ❌ `quatre-moins-un` ; interdit d'utiliser `-`
        - ❌ `2!` ; interdit d'utiliser `!`
        - ✅ `_`


## Une deuxième méthode avec correction à la volée
???+ question "Un QCM avec la réponse connue dès le choix"

    Question 1 : Quelle est la réponse à la question universelle ? Cocher deux réponses.
    
    {{ qcm([" $6\\times 7$ ", " $\\int_0^{42} 1 dx$ ", "Je ne sais pas", "La réponse D"], [1,2], shuffle = True) }}
    
    <br>    

    Question 2 : 1 + 1 = ? Cocher deux réponses.
    
    {{ qcm(["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2,4], shuffle = True) }}




## Une subtilité pour les *admonitions*:  

!!! note inline "Note à gauche"

    Texte de la note indenté  
    `!!! note inline "Note à gauche"`

Le texte non indenté se place à droite de la note.  
Il est en dehors de l'admonition.  
Il doit être assez long  
Pour que l'effet soit correct.  
...



... et de l'autre coté ...

!!! note inline end "Note à droite"

    Texte de la note indenté  
    `!!! note inline end "Note à droite"`
Le texte non indenté se place à droite de la note.  
Il est en dehors de l'admonition.  
Il doit être assez long  
également pour assurer un effet correct.  
