<!--- Tests pyodide
https://bouillotvincent.gitlab.io/pyodide-mkdocs/#modification-apportee 
{{ terminal() }}  {{ IDE() }}  {{ IDEv() }}     --->
# Insertion d'un mini IDE python grâce à Pyodide

## Exercice type
L'icône :material-play-circle: permet d'exécuter le programme. l'icône :material-check-circle: permet de l'évaluer et d'obtenir la réponse. <!--- https://pictogrammers.com/library/mdi/ --->
L'icône :material-reload: permet de réinitialiser l'exercice.
???+ question "IDE"
    {{ IDE('addition', MAX=3, SIZE = 10, SANS = "min, max") }}

Il est important de créer un script à compléter `nom_du_script.py`, une correction `nom_du_script-corr.py` et un fichier d'explication `nom_du_script-REM.md`.  
Ces fichiers doivent se trouver dans `docs/scripts/`.  
[Une source ici !](https://bouillotvincent.gitlab.io/pyodide-mkdocs/install_ide/#exemple){:target="_blank" }  
## Divers types d'insertion
???+ question "IDEv"
    {{ IDEv('exemple', MAX=3, SANS = "min, max") }}


???+ question "IDE"
    {{ IDE() }}


???+ question "terminal"
    {{ terminal() }}