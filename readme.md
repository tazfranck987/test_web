# Depôt de test pour publication de gitlab pages

:exclamation: Attention, il s'agit juste d'un dépôt de test. Il y a quelques fautes d'orthographe et erreurs non corrigées dans les pages. Notamment dans la page qui concerne pyodide ici:  
Il est important de créer un script à compléter `nom_du_script.py`, une correction `nom_du_script-corr.py` et un fichier d'explication `nom_du_script-REM.md`.  
  
il faut corriger en:  
Il est important de créer un script à compléter `nom_du_script.py`, une correction `nom_du_script_corr.py`, un fichier de test `nom_du_script_test.py` et un fichier d'explication `nom_du_script_REM.md`.  
  


## le .gitlab-ci.yml
Ce fichier a fonctionné pour la publication des pages automatique lors d'un push.  

```
image: python:latest
pages:
  stage: deploy
  only:
    - main
  script:
    - pip install -r requirements.txt
    - mkdocs build --site-dir public
  artifacts:
    paths:
      - public
```

## le requirements.txt
J'avais oublié d'intégrer le plugin `mkdocs-with-pdf`.  
En cas d'échec de la publication, aller voir les log dans les jobs.  

```
mkdocs-material>=8,<9
mkdocs-awesome-pages-plugin
mkdocs-macros-plugin
mkdocs-enumerate-headings-plugin
mkdocs-with-pdf
selenium
mkdocs-exclude-search
git+https://github.com/Epithumia/mkdocs-sqlite-console.git
```

## adresses
repo: https://gitlab.com/tazfranck987/test_web  
pages: https://tazfranck987.gitlab.io/test_web

